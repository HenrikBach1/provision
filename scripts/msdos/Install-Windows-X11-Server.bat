@echo off
@REM File: Install-Windows-X11-Server.bat

@REM TODO: Future: Check whether PKM Chocolatey exists otherwise install it via BoxStarter
@REM ...

@REM Install Windows X11-Server (Xlaunch)
sudo choco install -y vcxsrv 

@REM Print Default IPv4 address
@echo.*****************************************************************************
@REM .* Note:
@echo.*****************************************************************************
ipconfig | find "Default Gateway" | findstr "[0-9]"
@echo.1) NOTE: Remember to set above IPv4 address ^<ipv4^> into the environment variable
@echo.DISPLAY by the command:
@echo.set DISPLAY=^<ipv4^>:0.0
@echo.
@echo.2) NOTE: When launching Xlaunch (from Win-key) leave the defaults as is, except
@echo.enable: 'Disable access control'
@echo.*****************************************************************************
