@REM Install-Windows-sudo.bat

@REM FIXME: Why isn't the sudo package installed first time after BoxStarter?
@REM Try run below command: choco install -y sudo in the shell after install
set action=Installing sudo package...
@echo %action%
@echo %action% >> action.log
powershell ^
    -command ^
        start-process cmd -verb runas ^
            '^
                /C choco install -y sudo^
            '
set action=Installed sudo package.
@echo %action%
@echo %action% >> action.log
