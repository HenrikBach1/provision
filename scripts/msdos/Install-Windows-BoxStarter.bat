@echo off
set file=Install-Windows-BoxStarter.bat

set action= Started: %file%
echo %action%
echo %action% >> action.log

@REM This script installs packages:
@REM 1) Chocolatey(.org) and Chocolatey BoxStarter(.org)
@REM 2) sudo

@REM Download Boxstarter and execute setup.ps1
@REM Download BoxStarter zip file
@REM Unpack BoxStarter zip file
@REM Execute setup.ps1
powershell -NoProfile -ExecutionPolicy bypass ^
    -command ^
    (^
        (New-Object System.Net.WebClient).DownloadFile(^
            'https://boxstarter.org/downloads/Boxstarter.2.13.0.zip'^
            ,'./Boxstarter.2.13.0.zip'^
        )^
    )
powershell -NoProfile -ExecutionPolicy bypass ^
    -command ^
        (^
            Expand-Archive -Force -LiteralPath './Boxstarter.2.13.0.zip' -DestinationPath '.'^
        )
call setup.bat

echo.FIXME: Wait BoxStarter to finish (Enter on prompt in Powershell)...
pause

set action= Finished: %file%
echo %action%
echo %action% >> action.log
