#!/bin/bash
########
# Copyright (c) 2015+ Henrik Bach (henrik.bach@outlook.com). All rights reserved.
#

# bash arguments library: https://www.google.dk/webhp?sourceid=chrome-instant&ion=1&espv=2&ie=UTF-8#q=bash%20arguments%20library
if [[ ! x''${DEF_main}'' = x''1'' || x''${DEF_main}'' = x'''' ]]; then
echo "$0:"
echo "Version 20160107-1228"
fi

# Vocabulary
# . Get-Vocabulary.sh
include=source

# . SYNOPSIS(
# This wrapper script extends and complements the vagrant product with new options and behaviors
# )

vagrant_exe_path=$(dirname "$(readlink -f "$0")")/./
nop=$(echo "" > /dev/null)

if [[  x''help'' = x''$1''
    || x''--help'' = x''$1'' ]]; then
    echo "     nocleanup       leave temporary files for inspection"
fi
if [[  x''nocleanup'' = x''$1''
    || x''nocleanup'' = x''$2''
    || x''nocleanup'' = x''$3''
    || x''${ENV_DEBUG}'' = x''1''
    ]]; then
    nocleanup=1
    echo "nocleanup..."
    # # echo $($0 | grep -i nocleanup)
    # echo $*
    # exit 0
fi

########
# Request for Change:
########
# * Command: 'vagrant down': Should be equivalent with Command: 'vagrant halt'.
# * VAGRANT_NETWORK: New environment variable that controls the value of
#       property: config.vm.network "public_network".

########
# With this wrapper file, you may add your own VAGRANT_* environment variables
# or behavior to the vagrant tool.
#
# It may also inspire you to wrap other tools.
#
# Installation
# 1. Rename the executable <name>* to wrapped_<name>*
# 2. Copy this file to same folder as the original executable and rename
#       it to <name>,
#   or
#       set the path of the this file before the executable to be wrapped.
# 3. Test if the wrapper is working by invoking, ie <name> --version

if [[ ! x''${DEF_vagrant}'' = x''1'' || x''${DEF_vagrant}'' = x'''' ]]; then
    DEF_vagrant=1
    export DEF_vagrant
    [[ x''${ENV_DEBUG}'' = x''1'' ]] && echo "include: $0"

########
# Def. Constants
#

########
# Def. Functions
#

create_environment_2 () { # 1:ENV_ROOT
    ENV_ROOT=
    export ENV_ROOT
    if [[ x''${ENV_ROOT}'' = x'''' ]]; then
        # If not set, then choose default value
        ENV_ROOT=$1
        export ENV_ROOT

        if [[ x''${ENV_ROOT}'' = x'''' ]]; then
            # If not set, then choose default value
            ENV_ROOT=Vagrant
            export ENV_ROOT
        fi
    fi

    echo "Populate the environment..."

    if [[ ! (x''${ENV_SOURCING}'' = x''1'') ]]; then
        ENV_SOURCING=1
        export ENV_SOURCING
        [[ x''${ENV_DEBUG}'' = x''1'' ]] && echo "ENV_SOURCING=${ENV_SOURCING}"

        ENV_SOURCING=1
        export ENV_SOURCING

        echo "Traverse recursively to the ROOT (ENV_ROOT=${ENV_ROOT}) of Tree... or /"
        c=0
        while [[ ((! -d ./${ENV_ROOT})) || ((! -d /)) ]]; do
            if [[ x''$(pwd)'' = x''/'' ]]; then #Break, if root of FS is reached
                break
            fi

            pushd . > /dev/null
            cd ..
            c=`expr ${c} + 1`
            [[ x''${ENV_DEBUG}'' = x''1'' ]] && echo "c='"${c}"'"
        done

        for (( ; c >= 0; c-- )); do
            str=""
            if [[ -f ./environment_variables.sh ]]; then
                for (( i=$c; i > 0; i-- )); do
                    str="../${str}"
                done

                if (( ($c==0) )); then
                  str="./"
                fi

                echo "${str}environment_variables.sh"
                source ./environment_variables.sh 2> /dev/null
            fi

            if (( c > 0)); then
              popd > /dev/null
            fi
        done
        # popd > /dev/null

        # source ${onedrive}/HyperVisors/Vagrant/Instances/managed/OpenStack/.${HOSTNAME}/environment_variables.sh
        if [[ -f ./.${HOSTNAME}/environment_variables.sh ]]; then
            echo "./.${HOSTNAME}/environment_variables.sh"
            source ./.${HOSTNAME}/environment_variables.sh 2> /dev/null
        fi
    fi

    ENV_SOURCING=0
    export ENV_SOURCING
    [[ x''${ENV_DEBUG}'' = x''1'' ]] && echo "ENV_SOURCING=${ENV_SOURCING}"
}
# DEF_vagrant
fi

if [[ ! x''${DEF_main}'' = x''1'' || x''${DEF_main}'' = x'''' ]]; then
    DEF_main=1
    export DEF_main

$include $vagrant_exe_path/vagrant #todo: ?: create-environment-variables-file.sh

main() {
    [[ x''${ENV_DEBUG}'' = x''1'' ]] && echo "main: $0"

    echo "Prepare for work..."
    ENV_DEBUG=$VAGRANT_DEBUG
    export ENV_DEBUG

    # ENV_ROOT=Vagrant
    # export ENV_ROOT
    create_environment_2 Vagrant

    if [[ x''${VAGRANT_VAGRANTFILE}'' = x'''' ]]; then
        [[ x''${ENV_DEBUG}'' = x''1'' ]] && echo 'Debug: No Vagrantfile defined'
        VAGRANT_VAGRANTFILE_ORIG=Vagrantfile
        VAGRANT_VAGRANTFILE_TMP=.Vagrantfile
    else
        [[ x''${ENV_DEBUG}'' = x''1'' ]] && echo 'Debug: A Vagrantfile defined'
        VAGRANT_VAGRANTFILE_ORIG=${VAGRANT_VAGRANTFILE} # Save original value of Vagrantfile
        VAGRANT_VAGRANTFILE_TMP=.${VAGRANT_VAGRANTFILE}
    fi
    [[ x''${ENV_DEBUG}'' = x''1'' ]] && echo 'Debug: VAGRANT_VAGRANTFILE="'${VAGRANT_VAGRANTFILE}'"'

    export VAGRANT_VAGRANTFILE_ORIG
    export VAGRANT_VAGRANTFILE_TMP

    ########
    echo "Begin create header for globally defined Vagrant configuration...>>>"
    #
    rm -f ${PWD}/${VAGRANT_VAGRANTFILE_TMP} > /dev/null
cat <<EO_FILE >> ${PWD}/${VAGRANT_VAGRANTFILE_TMP}
VAGRANTFILE_API_VERSION = "${VAGRANT_API_VERSION}"
Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|
EO_FILE

    ########
    # Use not standardized Vagrant environment variables inside this block: >>>
    #
    if [[ x''${VAGRANT_SYNCED_FOLDER}'' != x'''' ]]; then
cat <<EO_FILE >> ${PWD}/${VAGRANT_VAGRANTFILE_TMP}
    config.vm.synced_folder ".", "/${VAGRANT_SYNCED_FOLDER}", id: "vagrant-root", enabled: true
EO_FILE
    fi
    #
    # Use not standardized Vagrant environment variables inside this block: <<<
    ########

cat <<EO_FILE >> ${PWD}/${VAGRANT_VAGRANTFILE_TMP}
end
EO_FILE
    ########
    echo "End create header for globally defined Vagrant configuration...<<<"
    #

    ########
    # Merge header with rest of the configuration file
    #
    if [[ -e ${PWD}/${VAGRANT_VAGRANTFILE_ORIG} ]]; then
        [[ x''${ENV_DEBUG}'' = x''1'' ]] && echo 'Debug: VAGRANT_VAGRANTFILE_ORIG="'${VAGRANT_VAGRANTFILE_ORIG}'"'
        cat ${PWD}/${VAGRANT_VAGRANTFILE_ORIG} >> ${PWD}/${VAGRANT_VAGRANTFILE_TMP}

        if [[ -e ${PWD}/.${HOSTNAME}/${VAGRANT_VAGRANTFILE_ORIG} ]]; then
          cat ${PWD}/.${HOSTNAME}/${VAGRANT_VAGRANTFILE_ORIG} >> ${PWD}/${VAGRANT_VAGRANTFILE_TMP}
        fi
    fi
    VAGRANT_VAGRANTFILE=${VAGRANT_VAGRANTFILE_TMP}
    export VAGRANT_VAGRANTFILE

    [[ x''${ENV_DEBUG}'' = x''1'' ]] && echo 'Debug: cmd="'wrapped_vagrant $*'"'
    if [[ x''${ENV_DEBUG}'' = x''1'' ]]; then
        # env | sort | grep -i "vagrant_ | env_"
        env | sort | grep -i "env_"
        env | sort | grep -i "vagrant_"
    fi

    echo "Working..."
    mkdir -p .${HOSTNAME}
    cd .${HOSTNAME}

    if [[ x''help'' = x''$1'' \
        || x''--help'' = x''$1'' ]]; then
        echo "     environment     shows used environment variables with values for vagrant wrapper"
    fi
    if [[ x''environment'' = x''$1'' ]]; then
        echo ""
        # env | sort | grep -i "vagrant_ | env_"
        env | sort | grep -i "env_"
        env | sort | grep -i "vagrant_"
        shift
    else
        wrapped_vagrant $*
    fi

    echo ""
    echo "If exists, immediately protect SSH private_key..."
    if [[ -e .${HOSTNAME}/.vagrant/machines/default/virtualbox/private_key ]]; then
        chmod 600 .${HOSTNAME}/.vagrant/machines/default/virtualbox/private_key
    fi
    if [[ ! $? -eq 0 ]]; then
        exit $?
    fi
    [[ x''${ENV_DEBUG}'' = x''1'' ]] && echo 'Debug: Exiting as requested...' && exit 0

    echo "Restore environment back to original..."
    VAGRANT_VAGRANTFILE=${VAGRANT_VAGRANTFILE_ORIG}
    export VAGRANT_VAGRANTFILE

    if [[ x''help'' = x''$1'' \
        || x''--help'' = x''$1'' ]]; then
        $nop
    fi
    if [[ x''init'' = x''$1'' ]]; then
        mv ./Vagrantfile ../
    fi

    if [[  x''$nocleanup'' = x''1'' ]]; then
        $nop
    else
        echo "Cleaning up..."
        rm -f ${PWD}/${VAGRANT_VAGRANTFILE_TMP} > /dev/null
        rm -f ../${VAGRANT_VAGRANTFILE_TMP} > /dev/null
        rm -f ./${VAGRANT_VAGRANTFILE_TMP} > /dev/null
    fi

    cd ..

    unset VAGRANT_VAGRANTFILE_ORIG
    unset VAGRANT_VAGRANTFILE_TMP
}
main $*
exit $?

# DEF_main
fi
