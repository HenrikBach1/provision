#!/bin/bash
FILE=provision-iotedge-builder-ubuntu-18.04.sh

container_name=iotedge-builder
image_family=ubuntu
image_version=18.04
image_tag=latest


#
# **************************************************
# Library code
#

image=$image_family:$image_version
container_name=$container_name-$image_family-$image_version


function debug
{
    #
    # https://askubuntu.com/questions/21136/how-to-debug-bash-script
    #
    if [[ $interactive == true ]]; then
        echo "#############|  Entering DEBUG mode  |####################";
        CMD=""
        while [[ $CMD != "exit" ]]; do
            read -p "> " CMD
            case $CMD in
                vars ) ( set -o posix ; set );;
                exit ) ;;
                * ) eval $CMD;;
            esac
        done
        echo "#############|  End of DEBUG mode |####################";
    fi
}

function RUN #$@
{
    # Avoid word splitting when using file operator
    run_cmd=$@
    if [[ $docker_in_exec == true ]]; then
        # You already run as root/sudo
        $exec sh -c "${run_cmd}"
    else
        $exec $sudo sh -c "${run_cmd}"
    fi
}


function enable_traceability
{
    if [[ $bash_traceability == true ]]; then
        set -x
        echo "***** bash traceability enabled!"
        echo "************************************************************"
    fi
}
function disable_traceability
{
    if [[ $bash_traceability == true ]]; then
        set +x
        echo "***** bash traceability disabled!"
        echo "************************************************************"
    fi
}


function enable_debug
{
    if [[ $bash_debugging == true ]]; then
        set -v
        echo "***** bash debugging enabled!"
        echo "************************************************************"
    fi
}
function disable_debug
{
    if [[ $bash_debugging == true ]]; then
        set +v
        echo "***** bash debugging disabled!"
        echo "************************************************************"
    fi
}


function enable_sudo
{
    if [[ $sudo_in_exec == true ]]; then
        sudo="sudo"
        echo "***** sudo_in_exec set!"
        echo "************************************************************"
    else
        sudo=""
        echo "***** sudo_in_exec not set!"
        echo "************************************************************"
    fi
}
function disable_sudo
{
    if [[ $sudo_in_exec == true ]]; then
        unset sudo
        echo "***** sudo_in_exec unset!"
        echo "************************************************************"
    fi
}


function enable_docker_in_exec
{
    if [[ $docker_in_exec == true ]]; then
        exec="docker exec $container_name "
        echo "***** Docker containerization enabled..."
        echo "************************************************************"
    else
        # TODO: Why not, anymore: exec="true &&"?
        exec=""
    fi
}
function disable_docker_in_exec
{
    unset exec
    echo "***** Docker containerization disabled..."
    echo "************************************************************"
}


function create_docker_dind_container
{
    docker container rm --force $container_name

    # https://hub.docker.com/_/ubuntu/
    docker container run -it -d \
        --name $container_name \
        -v ~/projects:/projects/ \
        -v /var/run/docker.sock:/var/run/docker.sock \
        $image
    echo "***** Docker-in-Docker containerizationing enabled for above $name..."
    echo "************************************************************"
}


function install_base_ubuntu
{
    echo "***** Installing software..."
    echo "************************************************************"

    RUN apt-get update

    RUN apt install -y \
        bridge-utils curl less lsb-release nano sudo wget unzip zip
}


function install_iotedge_builder_18_04
{
    echo "***** Installing iotedge builder software..."
    echo "************************************************************"
    #
    # https://github.com/Azure/iotedge/issues/162
    RUN apt install -y \
        openssl pkg-config libssl-dev cmake \
        libcurl4-openssl-dev debhelper dh-systemd uuid-dev \

    echo "***** Uninstalling rustc..."
    echo "************************************************************"
    #
    # http://www.admintome.com/blog/install-rust-on-ubuntu-18-04/
    # https://www.rust-lang.org/en-US/install.html
    RUN apt-get remove -y \
        rustc
    # echo "1" > curl https://sh.rustup.rs -sSf | sh
    # source $HOME/.cargo/env 

    echo "***** Installing Jq..."
    echo "************************************************************"
    #
    # https://stedolan.github.io/jq/download/
    RUN apt install -y \
        jq 

    echo "***** Installing Java..."
    echo "************************************************************"
    #
    RUN apt install -y \
        openjdk-11-jdk
}


function install_docker_ce_ubuntu_16_04
{
    install_docker_ce_ubuntu_16_04x18_10
}

function install_docker_ce_ubuntu_18_04
{
    install_docker_ce_ubuntu_16_04x18_10
}

function install_docker_ce_ubuntu_16_04x18_10
{
    #
    # https://docs.docker.com/install/linux/docker-ce/ubuntu/#set-up-the-repository
    #
    #   https://vitorbaptista.com/how-to-access-hosts-docker-socket-without-root
    #
    # To install Docker CE, you need the 64-bit version of one of these Ubuntu versions:
    #   Cosmic 18.10
    #   Bionic 18.04 (LTS)
    #   Xenial 16.04 (LTS)

    echo "***** Installing Docker CE..."
    echo "************************************************************"

    RUN apt-get update

    # Uninstall old versions
    RUN apt-get remove -y \
        docker docker-engine docker.io containerd runc

    # Setup to - Install using the repository
    RUN apt-get install -y \
        apt-transport-https \
        ca-certificates \
        curl \
        gnupg-agent \
        software-properties-common

    echo "***** Add Docker’s official GPG key"
    # TODO: Working:
    RUN "curl -fsSL https://download.docker.com/linux/ubuntu/gpg | $sudo apt-key add -"

    # Verify that you now have the key
    # RUN apt-key finger | grep 1092
    # RUN apt-key finger | grep CD88
    RUN apt-key finger 0EBFCD88

    # Set up the stable repository
    RUN "add-apt-repository 'deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable'"

    # Install Install Docker CE
    RUN apt-get update
    RUN apt-get install -y \
        docker-ce docker-ce-cli containerd.io
}


function install_dotnet_core_ubuntu_18_04
{
    #
    # https://dotnet.microsoft.com/download/linux-package-manager/ubuntu16-04/sdk-current
    #
    echo "***** Installing DotNet Core..."
    echo "************************************************************"

    # Register Microsoft key and feed
    RUN wget -q https://packages.microsoft.com/config/ubuntu/18.04/packages-microsoft-prod.deb
    RUN $sudo dpkg -i packages-microsoft-prod.deb

    # Install the .NET SDK
    RUN $sudo add-apt-repository universe
    RUN $sudo apt-get install -y apt-transport-https
    RUN $sudo apt-get update
    RUN $sudo apt-get install -y dotnet-sdk-2.2

    # Cleanup
    RUN rm -f packages-microsoft-prod.deb
}

function install_dotnet_core_ubuntu_16_04
{
    #
    # https://dotnet.microsoft.com/download/linux-package-manager/ubuntu16-04/sdk-current
    #
    echo "***** Installing DotNet Core..."
    echo "************************************************************"

    $exec wget -q https://packages.microsoft.com/config/ubuntu/16.04/packages-microsoft-prod.deb

    # Register the Microsoft repository GPG keys
    $exec sudo dpkg -i packages-microsoft-prod.deb

    # Install DotNet Core...
    $exec sudo apt-get update
    $exec sudo apt-get install -y apt-transport-https
    $exec sudo apt-get install -y dotnet-sdk-2.2

    $exec sudo rm -f packages-microsoft-prod.deb
}


function install_azure_cli_ubuntu_16_04
{
    install_azure_cli_ubuntu_16_04x18_04
}

function install_azure_cli_ubuntu_18_04
{
    install_azure_cli_ubuntu_16_04x18_04
}

function install_azure_cli_ubuntu_16_04x18_04
{
    #
    # https://docs.microsoft.com/en-us/cli/azure/install-azure-cli-apt?view=azure-cli-latest
    #
    # The current version of the CLI is 2.0.62.
    #
    # This package has been tested with:
    #   Ubuntu trusty, xenial (16.04), artful, and bionic (18.04)
    #   Debian wheezy, jessie, and stretch (9)
    #
    echo "***** Installing Azure CLI..."
    echo "************************************************************"

    RUN apt-get update

    # Get packages needed for the install process
    RUN apt-get install -y \
        apt-transport-https \
        curl \
        lsb-release \
        gpg

    # Download and install the Microsoft signing key
    # # TODO: Working:
    # cmd="curl -sL https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor | $sudo tee /etc/apt/trusted.gpg.d/microsoft.asc.gpg > /dev/null"
    # RUN "${cmd[@]}"
    RUN "curl -sL https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor | $sudo tee /etc/apt/trusted.gpg.d/microsoft.asc.gpg > /dev/null"
    
    # Verify key
    # RUN apt-key finger | grep 29CF
    RUN apt-key finger BE1229CF

    # Add the Azure CLI software repository
    # TODO: Working:
    # $exec sh -c "AZ_REPO=\$(lsb_release -cs) && \
    #             echo 'deb [arch=amd64] https://packages.microsoft.com/repos/azure-cli/ '''\$AZ_REPO''' main' | \
    #             $sudo tee /etc/apt/sources.list.d/azure-cli.list"
    # TODO: Working:
    # RUN "echo 'deb [arch=amd64] https://packages.microsoft.com/repos/azure-cli/ $(lsb_release -cs) main' | \
    #      $sudo tee /etc/apt/sources.list.d/azure-cli.list"
    # TODO: Working:
    RUN "AZ_REPO=\$(lsb_release -cs) && \
            echo 'deb [arch=amd64] https://packages.microsoft.com/repos/azure-cli/ '''\$AZ_REPO''' main' | \
            $sudo tee /etc/apt/sources.list.d/azure-cli.list"

    # Install the azure-cli package
    RUN apt-get update
    RUN apt-get install -y azure-cli
}


function install_powershell_ubuntu_18_04
{
    #
    # https://docs.microsoft.com/en-us/powershell/scripting/install/installing-powershell-core-on-linux?view=powershell-6
    #
    echo "***** Installing PowerShell..."
    echo "************************************************************"

    # Download the Microsoft repository GPG keys
    RUN wget -q https://packages.microsoft.com/config/ubuntu/18.04/packages-microsoft-prod.deb

    # Register the Microsoft repository GPG keys
    RUN $sudo dpkg -i packages-microsoft-prod.deb

    # Update the list of products
    RUN $sudo apt-get update

    # Enable the "universe" repositories
    RUN $sudo add-apt-repository universe

    # Install PowerShell
    RUN $sudo apt-get install -y powershell

    RUN rm -f packages-microsoft-prod.deb
}

function install_powershell_ubuntu_16_04
{
    #
    # https://docs.microsoft.com/en-us/powershell/scripting/install/installing-powershell-core-on-linux?view=powershell-6
    #
    echo "***** Installing PowerShell..."
    echo "************************************************************"

    # Download the Microsoft repository GPG keys
    $exec wget -q https://packages.microsoft.com/config/ubuntu/16.04/packages-microsoft-prod.deb

    # Register the Microsoft repository GPG keys
    $exec dpkg -i packages-microsoft-prod.deb

    # Install PowerShell...
    $exec apt-get update
    $exec apt-get install -y powershell

    $exec rm -f packages-microsoft-prod.deb
}


function create_user #$1=user
{
    RUN useradd --create-home --base-dir /home --shell /bin/bash $1
}


function link_group_to_user #$1=group $2=user
{
    RUN usermod -aG $1 $2
}


function link_folder_to_user #/projects myagent
{
    RUN rm /home/$2/$1 # Remove previous link...
    RUN ln -s $1 /home/$2
    # RUN chown $2:$2 /home/$2/$1
}


function main
{
    echo "***** Starts provisioning $container_name..."
    echo "************************************************************"

    if [[ $docker_in_exec == true ]]; then
        create_docker_dind_container
    fi

    install_base_ubuntu

    install_iotedge_builder_18_04
    install_docker_ce_ubuntu_18_04
    install_azure_cli_ubuntu_18_04
    install_powershell_ubuntu_18_04
    install_dotnet_core_ubuntu_18_04

    create_user myagent
    link_group_to_user docker myagent
    link_folder_to_user /projects myagent

    echo "***** Do Cleanup..."
    echo "************************************************************"
    #
    RUN apt autoremove

    echo "***** Finished provisioning $container_name..."
    echo "************************************************************"

    docker ps
}


#
# **************************************************
# Help handling code
#


function show_help
{
    # if [[ true ]]; then
    #     if [[
    #             -z $sudo_in_exec 
    #             || -z $docker_in_exec 
    #         ]]; then

            if [[
                        $1 == "-h"
                    || $1 == "--help"
                    || -z $1
                ]]; then
                echo "No parameters!..."
            else
                echo "Parameters!..."
                parameters=$@
                eval ${parameters[@]}
            fi

            if [[
                       "$1" == "--help"
                    || "$1" == "-h"
                    || (-z $sudo_in_exec || -z $docker_in_exec)
                ]]; then
                    echo "Control script behavior by these environment variables:"
                    echo "  sudo_in_exec=true       # Use 'sudo' in statement (required - $(echo $sudo_in_exec))."
                    echo "  docker_in_exec=false    # Use 'docker exec' in statement (required - $(echo $docker_in_exec))."
                    echo "  interactive=false       # Enable debuging in the shell ($(echo $interactive))."
                    echo "  bash_debugging=false    # Show statement before being evaluated (v)."
                    echo "  bash_traceability=false # Show statement after being evaluated (x)."
                    echo ""
                    echo "Examples:"
                    echo "  ./provision-iotedge-builder-ubuntu-18.04.sh 'sudo_in_exec=true && docker_in_exec=true'"
                    echo "  . ./provision-iotedge-builder-ubuntu-18.04.sh 'sudo_in_exec=true && docker_in_exec=true && interactive=true'"
                    echo "  sudo_in_exec=true && docker_in_exec=true && interactive=true . ./provision-iotedge-builder-ubuntu-18.04.sh"
                    help_is_shown=true
            fi
    #     fi
    # fi
}
show_help $@

#
# Handle execution in or population of functionality...
#
# https://stackoverflow.com/questions/2683279/how-to-detect-if-a-script-is-being-sourced#
#

(return 0 2>/dev/null) && sourced=1 || sourced=0

if [[
        #    ($help_is_shown != true || -z $help_is_shown)
           (-z "$help_is_shown")
        && ($sourced == 1) # Script being sourced...
    ]]; then
    #
    # Sourcing/Populating script into environment...
    #
    enable_sudo
    enable_docker_in_exec

    echo "***** Populated script (interactive=$(echo $interactive)) into Bash environment..."
    echo "************************************************************"
elif [[    
        #    ($help_is_shown != true || -z $help_is_shown)
           (-z $help_is_shown)
        && ($interactive == false || -z $interactive)
        && ($sourced == 0) # Script being spawned...
    ]]; then
    #
    # Execute script...
    #
    enable_sudo
    enable_docker_in_exec

    enable_traceability
    enable_debug

    main

    disable_sudo
    disable_docker_in_exec

    disable_debug
    disable_traceability
fi

unset help_is_shown
