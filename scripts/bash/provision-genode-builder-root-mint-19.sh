#!/bin/bash
# set -v
# set -x

FILE=provision-genode-builder-root-ubuntu-16.04.sh

# Inspired from
#   https://genode.org/documentation/genode-foundations-18-05.pdf

export project=genode
# export image_flavor=ubuntu
# export image_version=16.04
# export image=${image_flavor}:${image_version}
# export vm_name=${project}-${image_flavor}-${image_version}
export prj_dir=/projects/${project}

export wrk_dir=/
if [ x"$CMD" == x"" ]; then
    export CMD=sudo
fi

export WORKDIR="export wrk_dir="

export wrk_dir=/
# $WORKDIR /
$CMD apt-get update -y 

$CMD apt-get install -y \
            aptitude

$CMD apt-get install -y \
            gcc-7 gnat-7 autoconf2.64 \
            libsdl1.2-dev \
            # 
$CMD aptitude hold -y \
            gcc-7 gnat-7 autoconf2.64 \
            libsdl1.2-dev \
            #
$CMD apt-get install -y \
            build-essential \
            tclsh expect \
            qemu xorriso parted gdisk e2tools \
            byacc autoconf autogen bison flex g++ git gperf libxml2-utils subversion xsltproc \
            pkg-config libncurses-dev texinfo wget libexpat1-dev \
            git \
            nano emacs \
            sudo \
            #

#
# # Populate a volume with data
# #     https://stackoverflow.com/questions/37468788/what-is-the-right-way-to-add-data-to-an-existing-named-volume-in-docker
# docker run -v my-jenkins-volume:/data --name helper busybox true
# docker cp . helper:/data
# docker rm helper
# # -- % --
# docker run --rm -v $(pwd):/src -v my-jenkins-volume:/data busybox cp -r /src /data

# $CMD rm -rf ${prj_dir} 
$CMD git clone https://github.com/genodelabs/${project}.git ${prj_dir}

export wrk_dir=${prj_dir}
# $CMD export wrk_dir=${prj_dir} && mkdir -p ${prj_dir} && cd ${prj_dir}
# $WORKDIR ${prj_dir}

#
# Create tool-chain
#
$CMD ${wrk_dir}/./tool/create_builddir x86_64
$CMD ${wrk_dir}/./tool/tool_chain x86

export wrk_dir=${prj_dir}/build/x86_64
# $WORKDIR ${prj_dir}/build/x86_64

# $CMD /bin/bash -c "cd ${wrk_dir}; KERNEL=linux make run/demo"

# set +x
# set +v
