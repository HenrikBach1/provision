#!/bin/bash
FILE=clone-poky.sh

# . ./clone-poky.sh <branch>
# Future parameters
branch=rocko
branch=$1

#
echo Cloning the yocto poky reference distribution...
#

YP_PRJ_DIR=${HOME}/projects/yocto
mkdir -p ${YP_PRJ_DIR}/downloads

POKY_DIR=${YP_PRJ_DIR}/${branch}
mkdir -p ${POKY_DIR}
cd ${POKY_DIR}
git clone -b ${branch} https://git.yoctoproject.org/git/poky

#
echo Sourcing build environment...
#
PRJ_DIR=${YP_PRJ_DIR}/projects/yp-test/qemu/${branch}/qemux86
mkdir -p ${PRJ_DIR}
cd ${PRJ_DIR}
source ${POKY_DIR}/poky/oe-init-build-env .
