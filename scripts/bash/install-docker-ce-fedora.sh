#!/bin/bash

FILE=install-docker-ce-fedora.sh

# https://docs.docker.com/install/linux/docker-ce/fedora/#install-using-the-repository

sudo dnf remove -y docker \
                  docker-client \
                  docker-client-latest \
                  docker-common \
                  docker-latest \
                  docker-latest-logrotate \
                  docker-logrotate \
                  docker-selinux \
                  docker-engine-selinux \
                  docker-engine

sudo dnf -y install dnf-plugins-core
sudo dnf config-manager \
    --add-repo \
    https://download.docker.com/linux/fedora/docker-ce.repo

sudo dnf install -y docker-ce

sudo systemctl enable docker.service
sudo systemctl start docker.service
sudo systemctl status docker.service
