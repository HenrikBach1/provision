#!/bin/bash
FILE=install-openssh-root-ubuntu.sh
#
# Install OpenSSH server/service
# https://blog.ubuntu.com/2018/07/09/minimal-ubuntu-released
#
# Inspired by:
#   https://docs.docker.com/engine/examples/running_ssh_service/#run-a-test_sshd-container
#
USER_root=root
PWD_root=docker

apt-get update -y

apt-get install -y net-tools
echo ${USER_root}:${PWD_root} | chpasswd

apt-get install -y openssh-server
service --status-all | grep ssh

mkdir -p /var/run/sshd
sed -i 's/PermitRootLogin prohibit-password/PermitRootLogin yes/' /etc/ssh/sshd_config
sed -i 's/#PermitRootLogin yes/PermitRootLogin yes/' /etc/ssh/sshd_config

#
# Fix SSH login, otherwise, the user is kicked off after login
#
sed 's@session\s*required\s*pam_loginuid.so@session optional pam_loginuid.so@g' -i /etc/pam.d/sshd
service ssh start

#
# Fix SCP login, otherwise, the user is kicked off with "X cannot access Y"
#
