#!/bin/bash
FILE=provision-iotedge-builder-ubuntu-16.04.sh

container_name=iotedge-builder
image_family=ubuntu
image_version=16.04
image_tag=latest

#
# Behavior
#

sudo_in_exec=false      # Use 'sudo' in statement
docker_in_exec=true     # Use 'docker exec' in statement
$bash_debugging=false    # Show statement before executed
$bash_traceability=false # Show statement after exectuted
interactive=false       # Debug in shell

#
# **************************************************
# Help handling code
#

if [[ -n $1 ]]; then
    if [[ "$1" == "interactive=true" ]]; then
        export $1 # interactive=true
    fi

    if [[ \
            "$1" == "--help" \
            || "$1" == "-h" \
        ]]; then
        echo "interactive=true"
        return
    fi
fi

#
# **************************************************
# Library code
#

image=$image_family:$image_version
container_name=$container_name-$image_family-$image_version

echo "***** Starts provisioning $container_name..."
echo "************************************************************"


function enable_traceability
{
    if [ $bash_traceability == true ]; then
        set -x
        echo "***** bash traceability enabled!"
        echo "************************************************************"
    fi
}
function disable_traceability
{
    if [ $bash_traceability == true ]; then
        set +x
        echo "***** bash traceability disabled!"
        echo "************************************************************"
    fi
}


function enable_debug
{
    if [ $bash_debugging == true ]; then
        set -v
        echo "***** bash debugging enabled!"
        echo "************************************************************"
    fi
}
function disable_debug
{
    if [ $bash_debugging == true ]; then
        set +v
        echo "***** bash debugging disabled!"
        echo "************************************************************"
    fi
}


function enable_sudo
{
    if [ $sudo_in_exec == true ]; then
        sudo="sudo "
        echo "***** sudo_in_exec enabled!"
        echo "************************************************************"
    else
        sudo=""
        echo "***** sudo_in_exec disabled!"
        echo "************************************************************"
    fi
}
function disable_sudo
{
    if [ $sudo_in_exec == true ]; then
        sudo=""
        echo "***** sudo_in_exec disabled..."
        echo "************************************************************"
    fi
}


function enable_docker_in_exec
{
    if [ $docker_in_exec == true ]; then
        exec="docker exec $container_name "
        echo "***** Docker containerization enabled..."
        echo "************************************************************"
    fi
}

function disable_docker_in_exec
{
    exec="true && "
    echo "***** Docker containerization disabled..."
    echo "************************************************************"
}


function create_docker_dind_container
{
    docker container rm $container_name

    # https://hub.docker.com/_/ubuntu/
    docker container run -it -d \
        --name $container_name \
        -v ~/projects:/projects/ \
        -v /var/run/docker.sock:/var/run/docker.sock \
        $image
    echo "***** Docker-in-Docker containerizationing enabled for above $name..."
    echo "************************************************************"
}


function install_base_ubuntu
{
    echo "***** Installing software..."
    echo "************************************************************"
    #
    $exec apt-get update

    $exec apt install -y \
        curl less lsb-release nano sudo unzip wget
}


function install_iotedge_builder
{
    echo "***** Installing iotedge builder software..."
    echo "************************************************************"
    #
    # https://github.com/Azure/iotedge/issues/162
    $exec apt install -y \
        openssl pkg-config libssl-dev cmake \
        libcurl4-openssl-dev debhelper dh-systemd uuid-dev \

    echo "***** Uninstalling rustc..."
    echo "************************************************************"
    #
    # http://www.admintome.com/blog/install-rust-on-ubuntu-18-04/
    # https://www.rust-lang.org/en-US/install.html
    $exec apt-get remove -y \
        rustc
    # echo "1" > curl https://sh.rustup.rs -sSf | sh
    # source $HOME/.cargo/env 

    echo "***** Installing Jq..."
    echo "************************************************************"
    #
    # https://stedolan.github.io/jq/download/
    $exec apt install -y \
        jq 
}


function install_docker_ce_ubuntu_16.04
{
    echo "***** Installing Docker CE..."
    echo "************************************************************"
    #
    # https://docs.docker.com/install/linux/docker-ce/ubuntu/#set-up-the-repository
    $exec apt-get install -y \
        apt-transport-https \
        ca-certificates \
        curl \
        gnupg-agent \
        software-properties-common
    
    # TDOD: HB: Below line fails when installing in the container from command line.
    #           However, running the line manually in the container, succeeds.
    $exec "curl -fsSL https://download.docker.com/linux/ubuntu/gpg | $sudo apt-key add -"
    
    $exec apt-key fingerprint 0EBFCD88

    $exec add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"

    $exec apt-get update
    $exec apt-get install -y \
        docker-ce \
        docker-ce-cli \
        containerd.io
}


function install_dotnet_core_ubuntu_16.04
{
    echo "***** Installing DotNet Core..."
    echo "************************************************************"
    #
    # https://dotnet.microsoft.com/download/linux-package-manager/ubuntu16-04/sdk-current
    $exec wget -q https://packages.microsoft.com/config/ubuntu/16.04/packages-microsoft-prod.deb

    # Register the Microsoft repository GPG keys
    $exec sudo dpkg -i packages-microsoft-prod.deb

    # Install DotNet Core...
    $exec sudo apt-get update
    $exec sudo apt-get install -y apt-transport-https
    $exec sudo apt-get install -y dotnet-sdk-2.2

    $exec sudo rm -f packages-microsoft-prod.deb
}


function install_azure_cli_ubuntu_16.04
{
    echo "***** Installing Azure CLI..."
    echo "************************************************************"
    #
    # https://docs.microsoft.com/en-us/cli/azure/install-azure-cli-apt?view=azure-cli-latest
    # $exec sh -c "AZ_REPO=\$(lsb_release -cs) && echo 'deb [arch=amd64] https://packages.microsoft.com/repos/azure-cli/ '''\$(lsb_release -cs)''' main' | sudo tee /etc/apt/sources.list.d/azure-cli.list"
    $exec sh -c "AZ_REPO=\$(lsb_release -cs) && echo 'deb [arch=amd64] https://packages.microsoft.com/repos/azure-cli/ '''\$AZ_REPO''' main' | sudo tee /etc/apt/sources.list.d/azure-cli.list"

    $exec apt-get install -y \
        apt-transport-https software-properties-common

    $exec apt-key --keyring /etc/apt/trusted.gpg.d/Microsoft.gpg adv \
        --keyserver packages.microsoft.com \
        --recv-keys BC528686B50D79E339D3721CEB3E94ADBE1229CF

    $exec apt-get update
    $exec apt-get install azure-cli
}


function install_powershell_ubuntu_16.04
{
    echo "***** Installing PowerShell..."
    echo "************************************************************"
    #
    # https://docs.microsoft.com/en-us/powershell/scripting/install/installing-powershell-core-on-linux?view=powershell-6
    # Download the Microsoft repository GPG keys
    $exec wget -q https://packages.microsoft.com/config/ubuntu/16.04/packages-microsoft-prod.deb

    # Register the Microsoft repository GPG keys
    $exec dpkg -i packages-microsoft-prod.deb

    # Install PowerShell...
    $exec apt-get update
    $exec apt-get install -y powershell

    $exec rm -f packages-microsoft-prod.deb
}


function main
{
    echo "***** Do Work..."
    echo "************************************************************"
    #

    if [ $docker_in_exec == true ]; then
        create_docker_dind_container
    fi

    install_base_ubuntu
    install_iotedge_builder

    install_azure_cli_ubuntu_16.04
    install_powershell_ubuntu_16.04
    install_docker_ce_ubuntu_16.04
    install_dotnet_core_ubuntu_16.04

    echo "***** Do Cleanup..."
    echo "************************************************************"
    #
    $exec apt autoremove

    echo "***** Finished provisioning $container_name..."
    echo "************************************************************"

    docker ps

    # # TODO: HB:
    # #       * Create a user ie. myagent
    # #       * Check that the user has a home-dir with .bashrc etc in it
    # $exec useradd myagent
    # $exec echo myagent:myagent | chpasswd
    # #       * Add user to sudoers group
    # $exec usermod -aG sudo myagent
    # #       * Implement: https://vitorbaptista.com/how-to-access-hosts-docker-socket-without-root
    # $exec usermod -aG docker myagent
    # # TODO: HB: Test whether docker works!
    # $exec ls -halt /var/run/docker.sock
    # #       * Edit /etc/group appropriately
    # $exec ls -halt /var/run/docker.sock

    # # TODO: HB: 
    # #       * https://docs.docker.com/engine/reference/commandline/commit/
    # # docker commit -p iotedge-builder-ubuntu-16.04 henrikbach1/iotedge-builder-ubuntu-16.04:latest
    # # docker image ls | grep builder
}

enable_traceability
enable_debug

enable_sudo
enable_docker_in_exec

#
# Handle execution in or population of functionality...
#
if [ !$interactive == true ]; then
    main

    # TODO: If created Docker container, save it as an (remote) image...

    disable_sudo
    disable_docker_in_exec

    disable_debug
    disable_traceability
else
    echo "***** Populated bash environment..."
    echo "************************************************************"
fi
