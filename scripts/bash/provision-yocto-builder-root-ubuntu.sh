#!/bin/bash
#
FILE=provision-yocto-builder-root-ubuntu.sh
#

#
echo Info: Making Ubuntu distribution ready for installation of packages...
# *****************************************************************************
apt-get update -y

#
echo Info: Installing required packages...
# *****************************************************************************
#
echo Info: Packages for the Yocto Host Development System...
# http://www.yoctoproject.org/docs/latest/mega-manual/mega-manual.html#required-packages-for-the-host-development-system
#
# . ./install-yocto-host-packages-ubuntu.sh
#
apt-get install -y \
    gawk wget git-core diffstat unzip texinfo gcc-multilib \
    build-essential chrpath socat cpio python python3 python3-pip python3-pexpect \
    xz-utils debianutils iputils-ping

# Additional host packages required by poky/scripts/wic
apt-get install -y \
    curl dosfstools mtools parted syslinux tree

# Add "repo" tool (used by many Yocto-based projects)
curl http://storage.googleapis.com/git-repo-downloads/repo > /usr/local/bin/repo
chmod a+x /usr/local/bin/repo

#
echo Info: Fixing errors...
# *****************************************************************************
#

#
echo Info: Fixing error: "Please use a locale setting which supports utf-8."
# See https://wiki.yoctoproject.org/wiki/TipsAndTricks/ResolvingLocaleIssues
#
apt-get install -y \
    locales

sed -i -e 's/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen && \
     echo 'LANG="en_US.UTF-8"'>/etc/default/locale && \
     dpkg-reconfigure --frontend=noninteractive locales && \
     update-locale LANG=en_US.UTF-8

export LC_ALL=en_US.UTF-8
export LANG=en_US.UTF-8
export LANGUAGE=en_US.UTF-8
