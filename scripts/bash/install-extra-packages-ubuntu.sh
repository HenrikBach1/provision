#!/bin/bash

FILE=install-extra-packages-ubuntu.sh

$RUN echo Info: Adding other additional packages...
$RUN echo Info: Adding sudoers...
$RUN apt-get install -y \
    sudo

$RUN apt-get install -y \
    emacs nano screen \
    bash-completion
